# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

import spartan_messenger_pb2 as spartan__messenger__pb2


class SpartanMessengerStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.ChatRequest = channel.unary_unary(
        '/SpartanMessenger/ChatRequest',
        request_serializer=spartan__messenger__pb2.Message.SerializeToString,
        response_deserializer=spartan__messenger__pb2.Message.FromString,
        )
    self.SendMsg = channel.unary_unary(
        '/SpartanMessenger/SendMsg',
        request_serializer=spartan__messenger__pb2.Message.SerializeToString,
        response_deserializer=spartan__messenger__pb2.Message.FromString,
        )
    self.ReceiveMsg = channel.unary_stream(
        '/SpartanMessenger/ReceiveMsg',
        request_serializer=spartan__messenger__pb2.Message.SerializeToString,
        response_deserializer=spartan__messenger__pb2.Message.FromString,
        )


class SpartanMessengerServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def ChatRequest(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SendMsg(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def ReceiveMsg(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_SpartanMessengerServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'ChatRequest': grpc.unary_unary_rpc_method_handler(
          servicer.ChatRequest,
          request_deserializer=spartan__messenger__pb2.Message.FromString,
          response_serializer=spartan__messenger__pb2.Message.SerializeToString,
      ),
      'SendMsg': grpc.unary_unary_rpc_method_handler(
          servicer.SendMsg,
          request_deserializer=spartan__messenger__pb2.Message.FromString,
          response_serializer=spartan__messenger__pb2.Message.SerializeToString,
      ),
      'ReceiveMsg': grpc.unary_stream_rpc_method_handler(
          servicer.ReceiveMsg,
          request_deserializer=spartan__messenger__pb2.Message.FromString,
          response_serializer=spartan__messenger__pb2.Message.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'SpartanMessenger', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
