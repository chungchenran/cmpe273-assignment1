from __future__ import print_function

import grpc
import yaml
import sys

import spartan_messenger_pb2
import spartan_messenger_pb2_grpc

def accessConfig(dataType):
	with open("config.yaml", 'r') as stream:
		try:
			doc = yaml.load(stream)
		except yaml.YAMLError as e:
			print(e)
	return doc[dataType]

def availableUsers(username):
    userNameList = accessConfig("users")
    userNameList.remove(username)
    listString = ""
    for name in userNameList:
        listString = listString + name
        if name != userNameList[-1]:
            listString = listString + ","
    return listString

def run():
	username = sys.argv[1]
	chatPartner = ""
	sendChannel = grpc.insecure_channel('localhost:{}'.format(accessConfig("port")))
	receiveChannel = grpc.insecure_channel('localhost:{}'.format(accessConfig("port")))
	sendStub = spartan_messenger_pb2_grpc.SpartanMessengerStub(sendChannel)
	receiveStub = spartan_messenger_pb2_grpc.SpartanMessengerStub(receiveChannel)
	print("[Spartan] Connected to Spartan Server at port {}.".format(accessConfig("port")))
	print("[Spartan] User list: {}".format(availableUsers(username)))
	chatStatus = sendStub.ChatRequest(spartan_messenger_pb2.Message(senderid=username, receiverid="Spartan", message="requestConnect"))
	if chatStatus.message == "ChatOne":
		chatPartner = input("[{}] Enter a user whom you want to chat with: ".format(chatStatus.senderid))
		sendStub.ChatRequest(spartan_messenger_pb2.Message(senderid=username, receiverid="Spartan", message=chatPartner))
	else:
		chatPartner = chatStatus.message
		confirmConnect = input("[{}] {} is requesting to chat with you. Enter 'yes' to accept or different user: ".format(chatStatus.senderid, chatPartner))
		sendStub.ChatRequest(spartan_messenger_pb2.Message(senderid=username, receiverid="Spartan", message=confirmConnect))
	print("[Spartan] You are now ready to chat with {}.".format(chatPartner))
	while True:
		sendMessage = input("[{}] > ".format(username))
		sendStub.SendMsg(spartan_messenger_pb2.Message(senderid=username, receiverid=chatPartner, message=sendMessage))
		for message in receiveStub.ReceiveMsg(spartan_messenger_pb2.Message(senderid=username, receiverid="Spartan", message="checkMessages")):
			if message.senderid == chatPartner:
				print("[{}] {}".format(message.senderid, message.message))
				break


if __name__ == '__main__':
	run()