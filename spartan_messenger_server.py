from concurrent import futures
import time

import grpc
import yaml

import spartan_messenger_pb2
import spartan_messenger_pb2_grpc

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class SpartanMessengerServicer(spartan_messenger_pb2_grpc.SpartanMessengerServicer):
    def __init__(self):
        with open("config.yaml", 'r') as stream:
            try:
                self.doc = yaml.load(stream)
            except yaml.YAMLError as e:
                print(e)
        self.messageContainer = []
        self.firstUserMessageCacheCount = 0
        self.secondUserMessageCacheCount = 0
        self.listIndex = 0
        self.firstUser = ""
        self.secondUser = ""
        self.chatAccepted = False
        self.numOfUsers = 0

    def ChatRequest(self, request, context):
        if (request.message == "requestConnect") & (self.firstUser == ""):
            self.firstUser = request.senderid
            return spartan_messenger_pb2.Message(senderid="Spartan", receiverid=self.firstUser, message="ChatOne")
        elif self.firstUser == request.senderid:
            self.secondUser = request.message
            while True:
                if self.chatAccepted:
                    break
            return spartan_messenger_pb2.Message(senderid="Spartan", receiverid=self.firstUser, message="ChatValid")
        elif (request.message == "requestConnect") & (self.secondUser == request.senderid):
            return spartan_messenger_pb2.Message(senderid="Spartan", receiverid=self.secondUser, message=self.firstUser)
        elif (request.senderid == self.secondUser) & (request.message == "yes"):
            self.chatAccepted = True
            return spartan_messenger_pb2.Message(senderid="Spartan", receiverid=self.secondUser, message="ChatValid")

    def SendMsg(self, request, context):
        if request.senderid == self.firstUser:
            if self.firstUserMessageCacheCount < self.doc["max_num_messages_per_user"]:
                self.messageContainer.append(request)
                self.firstUserMessageCacheCount += 1
            else:
                for i, message in enumerate(self.messageContainer):
                    if message.senderid == self.firstUser:
                        self.messageContainer.pop(i)
                        break
                self.messageContainer.append(request)
                self.listIndex -= 1
        elif request.senderid == self.secondUser:
            if self.secondUserMessageCacheCount < self.doc["max_num_messages_per_user"]:
                self.messageContainer.append(request)
                self.secondUserMessageCacheCount += 1
            else:
                for i, message in enumerate(self.messageContainer):
                    if message.senderid == self.secondUser:
                        self.messageContainer.pop(i)
                        break
                self.messageContainer.append(request)
                self.listIndex -= 1
        return spartan_messenger_pb2.Message(senderid="", receiverid="", message="")

    def ReceiveMsg(self, request, context):
        if (len(self.messageContainer) > self.listIndex) & (request.message == "checkMessages"):
            newMessage = self.messageContainer[self.listIndex]
            if request.senderid == newMessage.receiverid:
                self.listIndex += 1
            yield newMessage
        else:
            yield spartan_messenger_pb2.Message(senderid="Spartan", receiverid=request.senderid, message="NoMessage")

def serve():
    with open("config.yaml", 'r') as stream:
        try:
            doc = yaml.load(stream)
        except yaml.YAMLError as e:
            print(e)
    portNumber = doc["port"]
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    spartan_messenger_pb2_grpc.add_SpartanMessengerServicer_to_server(SpartanMessengerServicer(), server)
    server.add_insecure_port('[::]:{}'.format(portNumber))
    server.start()
    print("Spartan server started on port {}.".format(portNumber))
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    serve()
